"use strict";
var mocha = require('mocha');
var expect = require('chai').expect;
var assert = require('chai').assert;
var Deck = require('../lib/deck');

describe('deck.js', () =>
{
    let deck = null;
    let card = null;
    
    describe('basic', () =>
    {
        beforeEach(() =>
        {
            deck = new Deck();
        });
        
        it('created a deck', () =>
        {
            expect(deck.cards.length).to.equal(52);
            expect(deck.cards[0]).to.eql({suit: "spades", rank: "ace"});
            expect(deck.cards[deck.cards.length-1]).to.eql({suit: "clubs", rank: "2"});
        });

        it('draws a card', () =>
        {
            for (let i = 1; i <= 52; i++) {
                card = deck.draw()
                expect(card).to.have.all.keys('suit', 'rank');
                expect(deck.cards).to.not.contain(card);
            }

            expect(deck.draw()).to.be.null;
            expect(deck.cards.length).to.equal(0);
        });

        it('inlays a card', () =>
        {
            card = deck.draw();
            expect(deck.cards).to.not.contain(card);
            expect(deck.cards.length).to.equal(51);
            deck.inlay(card);
            expect(deck.cards).to.contain(card);
            expect(deck.cards.length).to.equal(52);
            card = {suit: "spades", rank: "ace"};
            deck.inlay(card);
            expect(deck.cards).to.contain(card);
            expect(deck.cards.length).to.equal(53);
        });

        it('counts cards', () =>
        {
            for (let i = 52; i >= 1; i--) {
                expect(deck.count()).to.equal(i);
                deck.draw();
            }

            expect(deck.count()).to.equal(0);
            deck.draw();
            expect(deck.count()).to.equal(0);
        });

        it('shuffles', () =>
        {
            card = deck.draw();
            expect(deck.cards.length).to.equal(51);
            deck.shuffle();
            expect(deck.cards.length).to.equal(52);
            expect(deck.cards).to.contain(card);
        });
    });
    
    describe('advanced', () =>
    {
        it('manages multiple decks', () =>
        {
            for (let i = -2; i <= 1; i++) {
                deck = new Deck({multiply: i});
                expect(deck.count()).to.equal(52);
            }
            for (let i = 2; i <= 5; i++) {
                deck = new Deck({multiply: i});
                expect(deck.count()).to.equal(52 * i);
            }
        });

        it('extends the deck', () =>
        {
            deck = new Deck({
                multiply: 3,
                extend: [
                    {suit: "black", rank: "joker"},
                    {suit: "red", rank: "joker"},
                    {suit: "none", rank: "blank", limit: 1}
                ]
            });

            let totalCount = ((52 + 2) * 3) + 1;
            expect(deck.count()).to.equal(totalCount);

            let found = [0,0,0,0];
            for (let i = 0; i < totalCount; i++) {
                card = deck.draw();
                if (card.rank == "ace" && card.suit == "spades")
                    found[0]++;
                if (card.rank == "joker" && card.suit == "black")
                    found[1]++;
                if (card.rank == "joker" && card.suit == "red")
                    found[2]++;
                if (card.rank == "blank" && card.suit == "none")
                    found[3]++;
            }

            expect(found).to.eql([3,3,3,1]);
        });

        it('modifies the deck', () =>
        {
            let suits = ['spades', 'diamonds', 'clubs'];
            let ranks = ['raccoon', '10', '9', '8', '7', '6', '5', '4', '3', '2'];
            
            deck = new Deck({ suits: suits, ranks: ranks });
            expect(deck.count()).to.equal(30);

            for (let i = 1; i <= 30; i++) {
                card = deck.draw();
                expect(suits).to.contain(card.suit);
                expect(ranks).to.contain(card.rank);
            }
        });
    });
    
});
