interface Card
{
    suit: string;
    rank: string;
    limit?: number;
}

interface DeckOptions
{
    extend: Card[];
    suits: string[];
    ranks: string[];
    multiply: number;
}

class Deck
{
    private _opt: DeckOptions;

    cards: Card[];
    
    constructor (opt: Object = {})
    {
        this._opt = {
            extend: opt['extend'] || [],
            suits: opt['suits'] || ['spades', 'hearts', 'diamonds', 'clubs'],
            ranks: opt['ranks'] || ['ace', 'king', 'queen', 'jack', '10', '9', '8', '7', '6', '5', '4', '3', '2'],
            multiply: opt['multiply'] || 1
        };

        if (this._opt.multiply < 1)
            this._opt.multiply = 1;
        
        this.shuffle();
    }
    
    shuffle ()
    {
        this.cards = [];
        
        for (let i = 0; i < this._opt.multiply; i++) {
            for (let suit of this._opt.suits) {
                for (let rank of this._opt.ranks) {
                    this.inlay({ suit: suit, rank: rank });
                }
            }
            for (let card of this._opt.extend) {
                if (!card.limit || i < card.limit)
                    this.inlay({ suit: card.suit, rank: card.rank });
            }
        }
    }
    
    inlay (card: Card): boolean
    {
        if (card && card.suit && card.rank) {
            this.cards.push(card);
            return true;
        }
        else
            return false;
    }
    
    count (): number
    {
        return this.cards.length;
    }
    
    draw (): Card
    {
        let count = this.count();
        if (count > 0)
            return this.cards.splice(Math.floor(Math.random() * count), 1)[0] || null;
        else
            return null;
    }
}

var module = <any>module;
if (typeof module == "object" && typeof module.exports == "object")
    module.exports = Deck;
